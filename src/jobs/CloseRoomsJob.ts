import { CloseRoomsHelper } from '@/app/helpers'
import { Job } from '@/app/protocols'

export class CloseRoomsJob implements Job {
  handle(): void {
    CloseRoomsHelper()
  }
}

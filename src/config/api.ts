export const newMessageNotificationWehHook =
  process.env.SERVICE_API_WEBHOOK_NOTIFICATION || ''

export const baseURL = process.env.SERVICE_API_BASE_URL || ''

export const EndPoints = [
  { role: 'customer', auth: 'auth/customer', find: 'get-customer' },
  { role: 'shop', auth: 'auth/shop', find: 'get-shop' },
  { role: 'support', auth: 'auth/support', find: 'get-support' },
  { role: 'delivery', auth: 'auth/delivery', find: 'get-deliveryman' },
]

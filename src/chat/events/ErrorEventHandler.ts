import { SocketUnexpectedError } from '@/app/errors'
import { SocketContext } from '@/chat/protocols'
import { SocketErrorHandler } from '@/chat/helpers'

export async function ErrorEventHandler(context: SocketContext): Promise<void> {
  const { error } = context.payload

  return SocketErrorHandler(context, new SocketUnexpectedError(error))
}

import { SocketUnexpectedError } from '@/app/errors'
import Message, { MessageDocument } from '@/app/models/Message'
import Room from '@/app/models/Room'
import { SocketContext } from '@/chat/protocols'
import { SocketErrorHandler } from '@/chat/helpers'
import User, { UserDocument } from '@/app/models/User'
import { ServiceSendNotification } from '@/services/api'

export async function NewMessageEventHandler(
  context: SocketContext,
): Promise<void> {
  try {
    const { socket, channel, payload } = context
    const { roomId, message } = payload
    const { currentUser } = socket

    const room = await Room.findById(roomId)
    if (!room) throw new Error('Room not found')
    if (room.members.indexOf(currentUser._id) === -1)
      throw new Error('User not assigned to this room')

    const messagePayload = {
      content: message,
      from: currentUser._id,
      room: room._id,
    } as MessageDocument

    if (currentUser.shopkeeper) {
      messagePayload.extra.shopkeeper = currentUser.shopkeeper
    }

    const savedMessage = await Message.create(messagePayload)

    await Message.populate(savedMessage, {
      path: 'from',
      model: 'User',
      select: ['reference', 'name'],
    })

    const { from: fromMessage } = savedMessage as {
      from: UserDocument
    }

    room.messages.push(savedMessage._id)
    room.save()

    room.members.forEach(async member => {
      const userConnected = channel.adapter.rooms[member]
      if (userConnected) {
        channel.to(member).emit('newMessage', {
          _id: savedMessage._id,
          viewed: savedMessage.viewed,
          content: savedMessage.content,
          from: savedMessage.from,
          room: savedMessage.room,
          // eslint-disable-next-line eqeqeq
          fromCurrentUser: fromMessage.id == member,
          createdAt: savedMessage.createdAt,
          updatedAt: savedMessage.updatedAt,
        })
      } else {
        ServiceSendNotification({
          message: savedMessage,
          to: await User.findById(member),
        })
      }
    })
  } catch (err) {
    return SocketErrorHandler(context, new SocketUnexpectedError(err.message))
  }
}

import { SocketUnexpectedError } from '@/app/errors'
import Message from '@/app/models/Message'
import Room from '@/app/models/Room'
import { SocketContext } from '@/chat/protocols'
import { SocketErrorHandler } from '@/chat/helpers'

export async function MessagesViewedEventHandler(
  context: SocketContext,
): Promise<void> {
  try {
    const { socket, payload } = context
    const { roomId } = payload
    const { currentUser } = socket

    const room = await Room.findById(roomId)
    if (!room) throw new Error('Room not found')
    if (room.members.indexOf(currentUser._id) === -1)
      throw new Error('User not assigned to this room')

    const eventPayload = {
      roomId: room._id,
    }

    await Message.updateMany(
      {
        room: room._id,
        viewed: false,
        from: { $ne: currentUser._id },
      },
      {
        viewed: true,
      },
    )

    room.members.forEach(async member => {
      socket.broadcast.to(member).emit('messagesViewed', eventPayload)
    })
  } catch (err) {
    return SocketErrorHandler(context, new SocketUnexpectedError(err.message))
  }
}

import { SocketContext } from '@/chat/protocols'

export async function DisconnectingEventHandler(
  context: SocketContext,
): Promise<void> {
  const { socket } = context

  socket.leaveAll()
}

import { Socket } from 'socket.io'
import { AuthenticationHelper } from '@/app/helpers'
import { SocketAuthenticationError, SocketUnexpectedError } from '@/app/errors'

export async function Authentication(
  socket: Socket,
  next: Function,
): Promise<void> {
  try {
    const { Authorization, Identification } = socket.handshake.query

    const auth = await AuthenticationHelper(Authorization, Identification)

    if (auth.authorized) {
      socket.currentUser = auth.user
      if (auth.shopkeeper) {
        socket.currentUser.shopkeeper = auth.shopkeeper
      }
      return next(null, true)
    }

    return next(new SocketAuthenticationError(auth.reason), false)
  } catch (err) {
    return next(new SocketUnexpectedError(err.message), false)
  }
}

import { DataSocketError } from '.'

export interface SocketError {
  name: string
  data: DataSocketError
}

import { VerifyJwt } from '@/app/utils/'
import { InvalidTokenError, UnexpectedError } from '@/app/errors'

interface AuthResponse {
  authorized: boolean
  userId: string
  shopkeeper?: string
}

const IndexByRole = {
  customer: 'customer',
  shop: 'shopkeeper',
  delivery: 'delivery',
  support: 'support',
}

export async function AuthenticateFromRole(
  token: string,
  secret: string,
  role: string,
): Promise<AuthResponse> {
  try {
    const result = VerifyJwt(token, secret)
    if (!result) throw new InvalidTokenError()

    if (!result[IndexByRole[role]]) return undefined

    if (role === 'shop') {
      return {
        authorized: true,
        userId: undefined,
        shopkeeper: result[IndexByRole[role]],
      }
    }

    return {
      authorized: true,
      userId: result[IndexByRole[role]],
    }
  } catch (err) {
    throw new UnexpectedError(err)
  }
}

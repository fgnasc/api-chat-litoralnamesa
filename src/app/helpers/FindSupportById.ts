import User, { UserDocument } from '@/app/models/User'
import { ServiceFindSupportById } from '@/services/api'
import { NotFoundError } from '@/app/errors'

export async function FindSupportById(
  supportId: string,
): Promise<UserDocument> {
  let support = await User.findOne({
    'reference.id': supportId,
    'reference.type': 'support',
  })

  if (!support) {
    const supportData = await ServiceFindSupportById(supportId)

    if (!supportData) {
      throw new NotFoundError('Support')
    }

    support = await User.create({
      name: supportData.name,
      reference: { id: supportData.id, type: 'support' },
      role: 'support',
    })
  }

  return support
}

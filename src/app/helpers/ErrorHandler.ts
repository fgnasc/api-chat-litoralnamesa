import { ErrorProtocol, HttpResponse } from '@/app/protocols'

export function ErrorHandler(error: ErrorProtocol): HttpResponse {
  return {
    status: error.status || 500,
    body: {
      message: error.message,
      details:
        process.env.NODE_ENV !== 'production' && error.stack
          ? error.stack
          : undefined,
    },
  }
}

import User, { UserDocument } from '@/app/models/User'
import { ServiceFindShopById } from '@/services/api'
import { NotFoundError } from '@/app/errors'

export async function FindShopById(shopId: string): Promise<UserDocument> {
  let shop = await User.findOne({
    'reference.id': shopId,
    'reference.type': 'shop',
  })

  if (!shop) {
    const shopData = await ServiceFindShopById(shopId)

    if (!shopData) {
      throw new NotFoundError('Shop')
    }

    shop = await User.create({
      name: shopData.name,
      reference: { id: shopData.id, type: 'shop' },
      role: 'shop',
    })
  }

  return shop
}

import { ServiceFindOrderById } from '@/services/api'
import { OrderInterface } from '@/app/protocols'
import { NotFoundError } from '@/app/errors'
import { FindCustomerById, FindDeliveryById, FindShopById } from '.'

export async function FindOrderById(orderId: string): Promise<OrderInterface> {
  try {
    const order = await ServiceFindOrderById(orderId)

    const customer = await FindCustomerById(order.customer)

    const shop = await FindShopById(order.shop)

    const delivery = order.delivery
      ? await FindDeliveryById(order.delivery)
      : undefined

    return {
      id: order.id,
      customer,
      shop,
      delivery,
    }
  } catch (err) {
    if (err instanceof NotFoundError) {
      return undefined
    }
    throw err
  }
}

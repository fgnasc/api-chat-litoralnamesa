import Room, { RoomDocument } from '@/app/models/Room'

export async function FindRoomsByFilter(
  filters: object,
  page = 1,
  perPage = 20,
): Promise<Array<RoomDocument>> {
  const [result] = await Room.aggregate()
    .match(filters)
    .project({
      name: 1,
      open: 1,
      _id: 1,
      createdAt: 1,
      updatedAt: 1,
      members: 1,
      orderId: 1,
      countMessages: { $size: '$messages' },
    })
    .facet({
      metadata: [
        { $count: 'totalItems' },
        { $addFields: { currentPage: page } },
      ],
      data: [{ $skip: (page - 1) * perPage }, { $limit: perPage }],
    })

  const metadata = result.metadata[0] || {
    totalItems: 0,
    currentPage: page,
  }

  await Room.populate(result.data, { path: 'members' })

  const rooms = {
    ...metadata,
    totalPages: Math.ceil(metadata.totalItems / perPage),
    data: result.data,
  }

  return rooms
}

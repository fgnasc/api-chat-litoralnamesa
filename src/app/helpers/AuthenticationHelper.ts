import { UserDocument } from '@/app/models/User'
import { AuthenticationError } from '@/app/errors'
import {
  FindCustomerById,
  FindDeliveryById,
  FindShopById,
  FindSupportById,
  FindShopByShopkeeper,
  AuthenticateFromRole,
} from '.'

interface AuthenticationResult {
  authorized: boolean
  user?: UserDocument
  reason?: string
  shopkeeper?: string
}

export async function AuthenticationHelper(
  token: string,
  identifyToken: string,
): Promise<AuthenticationResult> {
  const IdentificationTokens = {
    delivery: {
      token: process.env.TOKEN_IDENTIFICATION_DELIVERY || '',
      secret: process.env.SECRET_AUTH_DELIVERY || '',
      findUserById: FindDeliveryById,
    },
    customer: {
      token: process.env.TOKEN_IDENTIFICATION_CUSTOMER || '',
      secret: process.env.SECRET_AUTH_CUSTOMER || '',
      findUserById: FindCustomerById,
    },
    support: {
      token: process.env.TOKEN_IDENTIFICATION_SUPPORT || '',
      secret: process.env.SECRET_AUTH_SUPPORT || '',
      findUserById: FindSupportById,
    },
    shop: {
      token: process.env.TOKEN_IDENTIFICATION_SHOP || '',
      secret: process.env.SECRET_AUTH_SHOP || '',
      findUserById: FindShopById,
    },
  }
  try {
    const role = Object.keys(IdentificationTokens).find(
      key => IdentificationTokens[key].token === identifyToken,
    )

    if (!role) {
      throw new AuthenticationError('Invalid Identification')
    }

    const auth = await AuthenticateFromRole(
      token,
      IdentificationTokens[role].secret,
      role,
    )

    if (auth && auth.authorized) {
      if (!IdentificationTokens[role].findUserById) {
        throw new Error('Identification Tokens not found')
      }

      let user
      if (auth.userId) {
        user = await IdentificationTokens[role].findUserById(auth.userId)
      } else if (auth.shopkeeper) {
        user = await FindShopByShopkeeper(auth.shopkeeper)
      }

      if (!user) {
        throw new Error('User not found')
      }
      return Promise.resolve({
        authorized: true,
        user,
        shopkeeper: auth.shopkeeper,
      })
    }

    throw new AuthenticationError('Invalid Authentication')
  } catch (err) {
    if (err instanceof AuthenticationError) {
      return Promise.resolve({
        authorized: false,
        reason: err.message,
      })
    }
    return Promise.reject(err)
  }
}

import User, { UserDocument } from '@/app/models/User'
import { ServiceFindDeliveryById } from '@/services/api'
import { NotFoundError } from '@/app/errors'

export async function FindDeliveryById(
  deliveryId: string,
): Promise<UserDocument> {
  let delivery = await User.findOne({
    'reference.id': deliveryId,
    'reference.type': 'delivery',
  })

  if (!delivery) {
    const deliveryData = await ServiceFindDeliveryById(deliveryId)

    if (!deliveryData) {
      throw new NotFoundError('Delivery')
    }

    delivery = await User.create({
      name: deliveryData.name,
      reference: { id: deliveryData.id, type: 'delivery' },
      role: 'delivery',
    })
  }

  return delivery
}

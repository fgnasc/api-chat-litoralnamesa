import User, { UserDocument } from '@/app/models/User'
import { ServiceFindShopkeeperById } from '@/services/api'
import {} from '.'
import { FindShopById } from './FindShopById'

export async function FindShopByShopkeeper(
  shopkeeperId: string,
): Promise<UserDocument> {
  let shop = await User.findOne({
    shopkeepers: { $elemMatch: { $eq: shopkeeperId } },
  })

  if (!shop) {
    const shopId = await ServiceFindShopkeeperById(shopkeeperId)
    shop = await FindShopById(shopId)
  }

  return shop
}

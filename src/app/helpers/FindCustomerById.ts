import User, { UserDocument } from '@/app/models/User'
import { ServiceFindCustomerById } from '@/services/api'
import { NotFoundError } from '@/app/errors'

export async function FindCustomerById(
  customerId: string,
): Promise<UserDocument> {
  let customer = await User.findOne({
    'reference.id': customerId,
    'reference.type': 'customer',
  })

  if (!customer) {
    const customerData = await ServiceFindCustomerById(customerId)

    if (!customerData) {
      throw new NotFoundError('Customer')
    }

    customer = await User.create({
      name: customerData.name,
      reference: { id: customerData.id, type: 'customer' },
      role: 'customer',
    })
  }

  return customer
}

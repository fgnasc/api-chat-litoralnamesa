import { UserDocument } from '@/app/models/User'

export interface OrderInterface {
  id: string
  customer: UserDocument
  delivery?: UserDocument
  shop: UserDocument
}

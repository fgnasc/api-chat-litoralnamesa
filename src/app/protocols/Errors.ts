export interface ErrorProtocol {
  status: number
  message: string
  stack?: string
}

import { ErrorProtocol } from '@/app/protocols'

export class InvalidTokenError extends Error implements ErrorProtocol {
  constructor() {
    super(`Invalid Token`)
    this.message = `Invalid Token`
    this.status = 400
    this.stack = new Error().stack
  }

  status: number

  stack: string
}

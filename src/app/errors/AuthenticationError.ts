export class AuthenticationError extends Error {
  message: string

  status: number

  stack: string

  constructor(reason: string) {
    super('Invalid authorization')
    this.message = reason
    this.status = 401
    this.stack = new Error().stack
  }
}

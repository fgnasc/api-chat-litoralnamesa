export class UnexpectedError extends Error {
  message: string

  status: number

  stack: string

  constructor(error: Error) {
    super('Unexpected Error occurred')
    this.message = error.message
    this.status = 500
    this.stack = error.stack
  }
}

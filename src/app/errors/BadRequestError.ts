import { ErrorProtocol } from '@/app/protocols'

export class BadRequestError extends Error implements ErrorProtocol {
  constructor(message: string) {
    super(`Forbidden Access`)
    this.message = message
    this.status = 400
    this.stack = new Error().stack
  }

  status: number

  stack: string
}

import { ErrorProtocol } from '@/app/protocols'

export class ForbiddenError extends Error implements ErrorProtocol {
  constructor() {
    super(`Forbidden`)
    this.message = `Forbidden Access`
    this.status = 403
    this.stack = new Error().stack
  }

  status: number

  stack: string
}

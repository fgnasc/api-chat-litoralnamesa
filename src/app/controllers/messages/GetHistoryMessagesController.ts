import { ControllerProtocol, HttpResponse } from '@/app/protocols'
import { Request } from 'express'
import Room from '@/app/models/Room'
import Message from '@/app/models/Message'
import { UnexpectedError, NotFoundError, ForbiddenError } from '@/app/errors'
import { ErrorHandler } from '@/app/helpers'
import { UserDocument } from '@/app/models/User'

export class GetHistoryMessagesController implements ControllerProtocol {
  async handle(req: Request): Promise<HttpResponse> {
    try {
      const room = await Room.findById(req.params.roomId)
      const { currentUser } = req

      if (!room) {
        return ErrorHandler(new NotFoundError('Room'))
      }

      if (
        room.members.indexOf(currentUser._id) === -1 &&
        currentUser.role !== 'support'
      ) {
        return ErrorHandler(new ForbiddenError())
      }

      const messagesHistory = await Message.find({
        room: req.params.roomId,
      }).populate('from', ['name', 'reference'])

      const messagesWithFromCurrentUser = messagesHistory.map(message => {
        const { from } = message as {
          from: UserDocument
        }

        const fromCurrentUser = from.id === currentUser.id

        return {
          _id: message._id,
          viewed: message.viewed,
          content: message.content,
          from: message.from,
          room: message.room,
          fromCurrentUser,
          createdAt: message.createdAt,
          updatedAt: message.updatedAt,
        }
      })

      return {
        status: 200,
        body: messagesWithFromCurrentUser,
      }
    } catch (err) {
      return ErrorHandler(new UnexpectedError(err))
    }
  }
}

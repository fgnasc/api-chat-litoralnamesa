import { ControllerProtocol, HttpResponse } from '@/app/protocols'
import { Request } from 'express'
import { UnexpectedError } from '@/app/errors'
import { ErrorHandler, FindRoomsByFilter, FindShopById } from '@/app/helpers'

export class GetRoomsByShopController implements ControllerProtocol {
  async handle(req: Request): Promise<HttpResponse> {
    try {
      const { page = '1', perPage = '20' } = req.query as {
        page: string
        perPage: string
      }

      const { shopId } = req.params

      const pageNum = parseInt(page, 0) || 1
      const perPageNum = parseInt(perPage, 0) || 20

      const shop = await FindShopById(shopId)

      const filters = {
        members: {
          $elemMatch: {
            $eq: shop._id,
          },
        },
      }

      const rooms = await FindRoomsByFilter(filters, pageNum, perPageNum)

      return {
        status: 200,
        body: rooms,
      }
    } catch (err) {
      return ErrorHandler(new UnexpectedError(err))
    }
  }
}

import { ControllerProtocol, HttpResponse } from '@/app/protocols'
import { Request } from 'express'
import Room from '@/app/models/Room'
import { FindDeliveryById, ErrorHandler } from '@/app/helpers'
import { NotFoundError, UnexpectedError } from '@/app/errors'

export class GetRoomByDeliveryController implements ControllerProtocol {
  async handle(req: Request): Promise<HttpResponse> {
    try {
      const { deliveryId } = req.params

      const delivery = await FindDeliveryById(deliveryId)

      if (!delivery) {
        return ErrorHandler(new NotFoundError('Delivery'))
      }

      const type = 'support_delivery'

      let room = await Room.findOne({
        members: { $elemMatch: { $eq: delivery._id } },
        type,
        open: true,
      })

      if (!room) {
        const { currentUser } = req

        const members = [currentUser._id, delivery._id]
        room = await Room.create({
          members,
          type,
          name: `Support with Delivery ${delivery.reference.id}`,
        })
      }

      await Room.populate(room, { path: 'members' })

      return {
        status: 200,
        body: room,
      }
    } catch (err) {
      return ErrorHandler(new UnexpectedError(err))
    }
  }
}

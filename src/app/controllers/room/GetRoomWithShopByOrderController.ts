import { ControllerProtocol, HttpResponse } from '@/app/protocols'
import { Request } from 'express'
import Room from '@/app/models/Room'
import { FindOrderById, ErrorHandler } from '@/app/helpers'
import { NotFoundError, UnexpectedError, ForbiddenError } from '@/app/errors'

export class GetRoomWithShopByOrderController implements ControllerProtocol {
  async handle(req: Request): Promise<HttpResponse> {
    try {
      const { currentUser, params } = req
      const { orderId } = params
      const type = ((): string => {
        if (currentUser.role === 'delivery') return 'delivery_order'
        if (currentUser.role === 'customer') return 'customer_order'
        return undefined
      })()

      if (!type) {
        throw new Error('Current User unexpected')
      }

      let room = await Room.findOne({
        orderId,
        type,
        open: true,
      })

      if (!room) {
        if (currentUser.role === 'support')
          return ErrorHandler(new NotFoundError('room'))
        const order = await FindOrderById(orderId)
        if (!order) return ErrorHandler(new NotFoundError('order'))

        if (
          currentUser.role === 'delivery' &&
          (!order.delivery || currentUser.id !== order.delivery.id)
        ) {
          return ErrorHandler(new ForbiddenError())
        }

        if (
          currentUser.role === 'customer' &&
          currentUser.id !== order.customer.id
        ) {
          return ErrorHandler(new ForbiddenError())
        }

        const members = [currentUser._id, order.shop._id]

        room = await Room.create({
          members,
          type,
          name: `Order ${order.id} - shop with ${currentUser.role}`,
          orderId,
        })
      }

      await Room.populate(room, { path: 'members' })

      return {
        status: 200,
        body: room,
      }
    } catch (err) {
      return ErrorHandler(new UnexpectedError(err))
    }
  }
}

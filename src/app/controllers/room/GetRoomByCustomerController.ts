import { ControllerProtocol, HttpResponse } from '@/app/protocols'
import { Request } from 'express'
import Room from '@/app/models/Room'
import { FindCustomerById, ErrorHandler } from '@/app/helpers'
import { NotFoundError, UnexpectedError } from '@/app/errors'

export class GetRoomByCustomerController implements ControllerProtocol {
  async handle(req: Request): Promise<HttpResponse> {
    try {
      const { customerId } = req.params

      const customer = await FindCustomerById(customerId)

      if (!customer) {
        return ErrorHandler(new NotFoundError('customer'))
      }

      let room = await Room.findOne({
        members: { $elemMatch: { $eq: customer._id } },
        type: 'support_customer',
        open: true,
      })

      if (!room) {
        const { currentUser } = req

        const members = [currentUser._id, customer._id]
        const type = 'support_customer'

        room = await Room.create({
          members,
          type,
          name: `Support with customer ${customer.reference.id}`,
        })
      }

      await Room.populate(room, { path: 'members' })

      return {
        status: 200,
        body: room,
      }
    } catch (err) {
      return ErrorHandler(new UnexpectedError(err))
    }
  }
}

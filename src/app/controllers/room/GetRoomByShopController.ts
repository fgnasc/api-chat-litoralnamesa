import { ControllerProtocol, HttpResponse } from '@/app/protocols'
import { Request } from 'express'
import Room from '@/app/models/Room'
import { FindShopById, ErrorHandler } from '@/app/helpers'
import { NotFoundError, UnexpectedError } from '@/app/errors'

export class GetRoomByShopController implements ControllerProtocol {
  async handle(req: Request): Promise<HttpResponse> {
    try {
      const { shopId } = req.params

      const shop = await FindShopById(shopId)

      if (!shop) {
        return ErrorHandler(new NotFoundError('Shop'))
      }

      const type = 'support_shop'

      let room = await Room.findOne({
        members: { $elemMatch: { $eq: shop._id } },
        type,
        open: true,
      })

      if (!room) {
        const { currentUser } = req

        const members = [currentUser._id, shop._id]
        room = await Room.create({
          members,
          type,
          name: `Support with Shop ${shop.reference.id}`,
        })
      }

      await Room.populate(room, { path: 'members' })

      return {
        status: 200,
        body: room,
      }
    } catch (err) {
      return ErrorHandler(new UnexpectedError(err))
    }
  }
}

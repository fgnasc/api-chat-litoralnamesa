import { ControllerProtocol, HttpResponse } from '@/app/protocols'
import { Request } from 'express'
import Room from '@/app/models/Room'
import { FindOrderById, ErrorHandler } from '@/app/helpers'
import {
  NotFoundError,
  UnexpectedError,
  ForbiddenError,
  BadRequestError,
} from '@/app/errors'

export class GetRoomWithDeliveryByOrderController
  implements ControllerProtocol {
  async handle(req: Request): Promise<HttpResponse> {
    try {
      const { currentUser, params } = req
      const { orderId } = params

      const type = ((): string => {
        if (currentUser.role === 'customer') return 'customer_order_delivery'
        if (currentUser.role === 'shop') return 'delivery_order'
        return undefined
      })()

      if (!type) {
        throw new Error('Current User unexpected')
      }

      let room = await Room.findOne({
        orderId,
        type,
        open: true,
      })

      if (!room) {
        const order = await FindOrderById(orderId)
        if (!order) return ErrorHandler(new NotFoundError('order'))

        if (
          currentUser.role === 'customer' &&
          currentUser.id !== order.customer.id
        ) {
          return ErrorHandler(new ForbiddenError())
        }

        if (currentUser.role === 'shop' && currentUser.id !== order.shop.id) {
          return ErrorHandler(new ForbiddenError())
        }

        if (!order.delivery) {
          return ErrorHandler(
            new BadRequestError("This order haven't a delivery man"),
          )
        }

        const members = [currentUser._id, order.delivery._id]

        room = await Room.create({
          members,
          type,
          name: `Order ${order.id} - delivery with ${currentUser.role}`,
          orderId,
        })
      }

      await Room.populate(room, { path: 'members' })

      return {
        status: 200,
        body: room,
      }
    } catch (err) {
      return ErrorHandler(new UnexpectedError(err))
    }
  }
}

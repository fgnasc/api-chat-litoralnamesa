import jwt from 'jsonwebtoken'

export function VerifyJwt(token: string, secret = ''): string | object {
  try {
    if (!token) return undefined
    return jwt.verify(token, secret)
  } catch (e) {
    return undefined
  }
}

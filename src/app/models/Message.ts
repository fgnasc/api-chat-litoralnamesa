import mongoose, { Document, Schema } from 'mongoose'
import { UserDocument } from '@/app/models/User'
import { RoomDocument } from '@/app/models/Room'

export interface MessageDocument extends Document {
  content: string
  from: string | UserDocument
  room: string | RoomDocument
  extra?: {
    shopkeeper: string
  }
  viewed: boolean
  createdAt: string
  updatedAt: string
}

const MessageSchema = new Schema(
  {
    content: { type: String, required: true },
    from: { type: Schema.Types.ObjectId, ref: 'User' },
    room: { type: Schema.Types.ObjectId, ref: 'Room' },
    extra: {
      shopkeeper: { type: String },
    },
    viewed: { type: Boolean, default: false },
  },
  {
    timestamps: true,
  },
)

export default mongoose.model<MessageDocument>('Message', MessageSchema)

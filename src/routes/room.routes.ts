import { Router } from 'express'
import {
  GetRoomWithShopByOrderController,
  GetRoomController,
  GetRoomByCustomerController,
  GetRoomWithCustomerByOrderController,
  GetRoomWithDeliveryByOrderController,
  GetRoomByShopController,
  GetRoomByDeliveryController,
} from '@/app/controllers/room'
import messagesRoutes from './messages.routes'
import {
  NotIsShop,
  IsSupport,
  NotIsSupport,
  NotIsCustomer,
  NotIsDelivery,
} from './middlewares'

const roomRoutes = Router()

roomRoutes.get('/:roomId', async (req, res) => {
  const controller = new GetRoomController()
  const response = await controller.handle(req)
  return res.status(response.status).json(response.body)
})

roomRoutes.get(
  '/order/:orderId/shop',
  NotIsShop,
  NotIsSupport,
  async (req, res) => {
    const controller = new GetRoomWithShopByOrderController()
    const response = await controller.handle(req)
    res.status(response.status).json(response.body)
  },
)

roomRoutes.get(
  '/order/:orderId/customer',
  NotIsCustomer,
  NotIsSupport,
  async (req, res) => {
    const controller = new GetRoomWithCustomerByOrderController()
    const response = await controller.handle(req)
    res.status(response.status).json(response.body)
  },
)

roomRoutes.get(
  '/order/:orderId/delivery',
  NotIsDelivery,
  NotIsSupport,
  async (req, res) => {
    const controller = new GetRoomWithDeliveryByOrderController()
    const response = await controller.handle(req)
    res.status(response.status).json(response.body)
  },
)

roomRoutes.get('/customer/:customerId', IsSupport, async (req, res) => {
  const controller = new GetRoomByCustomerController()
  const response = await controller.handle(req)
  res.status(response.status).json(response.body)
})

roomRoutes.get('/shop/:shopId', IsSupport, async (req, res) => {
  const controller = new GetRoomByShopController()
  const response = await controller.handle(req)
  res.status(response.status).json(response.body)
})

roomRoutes.get('/delivery/:deliveryId', IsSupport, async (req, res) => {
  const controller = new GetRoomByDeliveryController()
  const response = await controller.handle(req)
  res.status(response.status).json(response.body)
})

roomRoutes.use('/messages', messagesRoutes)

export default roomRoutes

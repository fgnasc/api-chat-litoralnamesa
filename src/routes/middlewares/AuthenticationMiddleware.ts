import { Response, Request } from 'express'
import { AuthenticationHelper, ErrorHandler } from '@/app/helpers'

export async function Authentication(
  request: Request,
  response: Response,
  next: Function,
): Promise<void | Response> {
  try {
    const { identification, authorization } = request.headers

    const auth = await AuthenticationHelper(
      authorization,
      identification as string,
    )

    if (auth.authorized) {
      request.currentUser = auth.user
      if (auth.shopkeeper) {
        request.currentUser.shopkeeper = auth.shopkeeper
      }
      return next()
    }
    return response.status(401).json({ reason: auth.reason })
  } catch (err) {
    const result = ErrorHandler(err)
    return response.status(result.status || 500).json(result.body)
  }
}

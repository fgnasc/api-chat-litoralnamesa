import { Router } from 'express'
import {
  GetRoomsController,
  GetRoomsByCustomerController,
  GetRoomsByDeliveryController,
  GetRoomsByShopController,
  GetRoomsByOrderController,
} from '@/app/controllers'
import { IsSupport } from './middlewares'

const roomsRoutes = Router()

roomsRoutes.get('/', async (req, res) => {
  const controller = new GetRoomsController()
  const response = await controller.handle(req)
  return res.status(response.status).json(response.body)
})

roomsRoutes.get('/customer/:customerId', IsSupport, async (req, res) => {
  const controller = new GetRoomsByCustomerController()
  const response = await controller.handle(req)
  return res.status(response.status).json(response.body)
})

roomsRoutes.get('/delivery/:deliveryId', IsSupport, async (req, res) => {
  const controller = new GetRoomsByDeliveryController()
  const response = await controller.handle(req)
  return res.status(response.status).json(response.body)
})

roomsRoutes.get('/shop/:shopId', IsSupport, async (req, res) => {
  const controller = new GetRoomsByShopController()
  const response = await controller.handle(req)
  return res.status(response.status).json(response.body)
})

roomsRoutes.get('/order/:orderId', IsSupport, async (req, res) => {
  const controller = new GetRoomsByOrderController()
  const response = await controller.handle(req)
  return res.status(response.status).json(response.body)
})

export default roomsRoutes

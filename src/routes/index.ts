import { Router } from 'express'
import RoomsRoutes from './rooms.routes'
import RoomRoutes from './room.routes'

import { Authentication } from './middlewares'

const router = Router()

router.use('/rooms', Authentication, RoomsRoutes)

router.use('/room', Authentication, RoomRoutes)

export default router

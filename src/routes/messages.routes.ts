import { Router } from 'express'
import { GetHistoryMessagesController } from '@/app/controllers'

const messagesRoutes = Router()

messagesRoutes.get('/:roomId', async (req, res) => {
  const controller = new GetHistoryMessagesController()
  const response = await controller.handle(req)
  res.status(response.status).json(response.body)
})

export default messagesRoutes

import { MessageDocument } from '@/app/models/Message'
import { UserDocument } from '@/app/models/User'
import { newMessageNotificationWehHook } from '@/config/api'
import Api from './plugins/AxiosApi'

interface NotificationPayload {
  message: MessageDocument
  to: UserDocument
}

export function ServiceSendNotification(payload: NotificationPayload): void {
  const { message, to } = payload

  Api.post(newMessageNotificationWehHook, {
    message: {
      content: message.content,
      from: message.from,
    },
    to: {
      type: to.reference.type,
      id: to.reference.id,
      name: to.name,
    },
  }).catch(reason => {
    // eslint-disable-next-line no-console
    console.log(
      `Houve um erro ao acessar o webhook ${newMessageNotificationWehHook}\n Reason: ${reason}`,
    )
  })
}

import api from '@/services/api/plugins/AxiosApi'
import { ServiceFailed } from '@/services/api/errors'

interface Order {
  id: string
  customer: string
  delivery?: string
  shop: string
}

export async function ServiceFindOrderById(orderId: string): Promise<Order> {
  try {
    const { data } = await api.get(`get-order/${orderId}`)

    const { order, error } = data

    if (error) return undefined

    return {
      id: orderId,
      customer: order.IDCustomer,
      delivery: order.IDSystemUser || undefined,
      shop: order.IDCompany,
    }
  } catch (err) {
    throw new ServiceFailed(err)
  }
}

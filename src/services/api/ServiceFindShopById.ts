import api from '@/services/api/plugins/AxiosApi'
import { ServiceFailed } from '@/services/api/errors'

interface User {
  id: string
  name: string
}

export async function ServiceFindShopById(id: string): Promise<User> {
  try {
    const { data: shop } = await api.get(`get-shop/${id}`)

    if (shop.error) return undefined

    return {
      name: shop.name,
      id,
    }
  } catch (err) {
    throw new ServiceFailed(err)
  }
}

import api from '@/services/api/plugins/AxiosApi'
import { ServiceFailed } from '@/services/api/errors'

export async function ServiceFindShopkeeperById(id: string): Promise<string> {
  try {
    const { data } = await api.get(`get-support/${id}`)

    const { shopkeeper, error } = data

    if (error) return undefined

    return shopkeeper.IDCompany || undefined
  } catch (err) {
    throw new ServiceFailed(err)
  }
}

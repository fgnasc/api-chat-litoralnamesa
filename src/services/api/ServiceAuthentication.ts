// import api from '@/plugins/AxiosApi'
import { AuthenticationError } from '@/app/errors'

interface AuthenticationResult {
  authorized: boolean
  userId?: string
}

const IdsExample = {
  support: [300, 301, 302, 303, 304, 305],
  delivery: [618, 154, 344, 341, 503, 305],
  customer: [17443, 43149, 40003, 43151, 35052, 30514],
  shop: [641, 154, 316, 375, 529, 328],
}

export async function ServiceAuthentication(
  token: string,
  role: string,
): Promise<AuthenticationResult> {
  if (token) {
    try {
      throw new Error('service unavailable')
    } catch (err) {
      if (err.message === 'service unavailable') {
        if (IdsExample[role]) {
          return {
            authorized: true,
            userId: token,
          }
        }
      }
      throw new Error('Authentication service unavailable')
    }
  }
  throw new AuthenticationError('Token not provided')
}

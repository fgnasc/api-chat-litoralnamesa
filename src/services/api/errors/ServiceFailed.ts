import { AxiosError } from 'axios'

export class ServiceFailed extends Error {
  message: string

  status: number

  stack: string

  constructor(error: AxiosError) {
    super(error.isAxiosError ? 'Connection Failed' : 'Service Failed')
    if (error.isAxiosError) {
      this.message = 'An error occurred while connect to API service'
      this.status = 502
    } else {
      this.message = 'An unexpected error occurred'
      this.status = 500
    }
    this.stack = error.stack
  }
}

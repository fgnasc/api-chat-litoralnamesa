import api from '@/services/api/plugins/AxiosApi'
import { ServiceFailed } from '@/services/api/errors'

interface User {
  id: string
  name: string
}

export async function ServiceFindSupportById(id: string): Promise<User> {
  try {
    const { data } = await api.get(`get-support/${id}`)

    const { support, error } = data

    if (error) return undefined

    return {
      name: support.DisplayName,
      id,
    }
  } catch (err) {
    throw new ServiceFailed(err)
  }
}

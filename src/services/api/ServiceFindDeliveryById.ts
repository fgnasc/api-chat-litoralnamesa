import api from '@/services/api/plugins/AxiosApi'
import { ServiceFailed } from '@/services/api/errors'

interface User {
  id: string
  name: string
}

export async function ServiceFindDeliveryById(id: string): Promise<User> {
  try {
    const { data } = await api.get(`get-deliveryman/${id}`)

    const { deliveryman, error } = data

    if (error) return undefined

    return {
      name: deliveryman.Name,
      id,
    }
  } catch (err) {
    throw new ServiceFailed(err)
  }
}

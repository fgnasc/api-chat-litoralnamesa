import api from '@/services/api/plugins/AxiosApi'
import { ServiceFailed } from '@/services/api/errors'

interface User {
  id: string
  name: string
}

export async function ServiceFindCustomerById(id: string): Promise<User> {
  try {
    const { data } = await api.get(`get-customer/${id}`)

    const { customer, error } = data

    if (error) return undefined

    return {
      name: customer.Name,
      id,
    }
  } catch (err) {
    throw new ServiceFailed(err)
  }
}

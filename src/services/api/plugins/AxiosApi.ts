import axios from 'axios'
import { baseURL } from '@config/api'

export default axios.create({
  baseURL,
  headers: {
    'X-Api-Token': process.env.SERVICE_API_TOKEN,
  },
  timeout: 6 * 1000,
})

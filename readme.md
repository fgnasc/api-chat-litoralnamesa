# Litoral Chat Api

API utilizada para manipular um sistema de CHAT entre usuários de 4 tipos distintos (Customers, Shops/Shopkeepers, Deliverymen, Admins).

## Sumário

- [Requisitos](#requisitos)
- [Configuração](#configuração)
  - [Comandos Disponíveis](#comandos-disponíveis)
  - [Variáveis de Ambiente](#variáveis-de-ambiente)
- [Autenticação](#autenticação)
- [Rotas da API](#rotas-da-api)
  - [Find all rooms](#get---rooms)
  - [Find rooms by Customer (exclusivo Admin)](#get---roomscustomercustomerid)
  - [Find rooms by Shop (exclusivo Admin)](#get---roomsshopshopid)
  - [Find rooms by Delivery Man (exclusivo Admin)](#get---roomsdeliverydeliveryid)
  - [Find rooms by Order (exclusivo Admin)](#get---roomsorderorderid)
  - [Find room by ID](#get---roomroomid)
  - [Find room with Shop by Order](#get---roomorderorderidshop)
  - [Find room with Customer by Order](#get---roomorderorderidcustomer)
  - [Find room with Delivery Man by Order](#get---roomorderorderiddelivery)
  - [Find room with Customer by ID (exclusivo Admin)](#get---roomcustomercustomerid)
  - [Find room with Shop by ID (exclusivo Admin)](#get---roomshopshopid)
  - [Find room with Delivery Man by ID (exclusivo Admin)](#get---roomdeliverydeliveryid)
  - [Find history messages by Room](#get---roommessagesroomid)
- [Socket Server](#socket-server)
  - [Autenticação](#autenticação-1)
  - [Eventos Emitidos](#eventos-emitidos)
    - [newMessage](#event-name-newmessage)
    - [messagesViewed](#event-name-messagesviewed)
    - [error](#event-name-error)
  - [Eventos Esperados](#eventos-esperados)
    - [newMessage](#event-name-newMessage-1)
    - [messagesViewed](#event-name-messagesviewed-1)
- [Observações](#observações)

## Requisitos

O projeto foi desenvolvido utilizando as tecnologias:

- Typescript:**4.0.2**
- NodeJs:**14**
- MongoDB:**4.2**

## Configuração

Antes de tentar executar o projeto, é necessário instalar todas as suas dependências, para isso execute:

``yarn install``

ou

``npm install``


### Comandos disponíveis:

``npm run build`` ou ``yarn build`` - Transpila os arquivos em Typescript para Javascript e adiciona eles dentro da pasta **dist**.

``npm run start`` ou ``yarn start`` - Inicia o servidor na porta específica a partir dos arquivos gerados na pasta **dist** pelo comando **build**.

``npm run dev:serve`` ou ``yarn dev:server`` - Executa o servidor em modo de desenvolvimento utilizando o próprio typescript.

### Variáveis de Ambiente:

Todas as variáveis de ambiente estão descritas no arquivo .env.example:

- NODE_ENV = development | production
- PORT = Porta onde o servidor iniciará
- TOKEN_IDENTIFICATION_CUSTOMER = Token para identificar o app do: Customer
- TOKEN_IDENTIFICATION_SHOP = Token para identificar o app do: Shop
- TOKEN_IDENTIFICATION_SUPPORT = Token para identificar o app do: Support
- TOKEN_IDENTIFICATION_DELIVERY = Token para identificar o app do Deliveryman
- DB_MONGO_URL = Url de conexão com o mongodb
- SERVICE_API_BASE_URL = Url base do serviço da API
- SERVICE_API_TOKEN = token de autenticação que será utilizado para se conectar ao serviço da API
- SERVICE_API_WEBHOOK_NOTIFICATION = Rota que será chamada quando algum usuário que não está online receber alguma mensagem.
- SECRET_AUTH_CUSTOMER = segredo utilizado para validar o JWT do customer
- SECRET_AUTH_SHOP = segredo utilizado para validar o JWT do shopkeeper
- SECRET_AUTH_SUPPORT = segredo utilizado para validar o JWT do admin
- SECRET_AUTH_DELIVERY = segredo utilizado para validar o JWT do deliveryman
- CHAT_ALLOWED_ORIGINS = Origens que podem se conectar a esta API, caso nenhuma seja informada, todas poderão.

## Autenticação

Tanto as rotas da API como a conexão via socket necessitam de **2 tokens** para realizar a autenticação:

- **Identification**: Deve conter um dos TOKEN_IDENTIFICATION definidos nas variáveis de ambiente. Serve para a identificação de qual tipo de usuário está se conectando ao serviço.
- **Authorization**: Deve contar um JWT válido identificando qual o usuário que está se conectando.

## Rotas da API

**Headers:**
```
Identification:
Authorization:
```

### GET - /rooms

Rota para consultar todas as salas de conversa que estão em aberto. Cada usuário só pode verificar as salas de qual faz parte, exceto se este foi um administrador.

**Query Params:**
```
page: (default 1)
perPage: (default 20)
```

**Response Example:**
```
{
  "totalItems": 6,
  "currentPage": 1,
  "totalPages": 6,
  "data": [
    {
      "_id": "5f7a168531ae2d018d89b7a0",
      "members": [
        {
          "reference": {
            "id": "341",
            "type": "delivery"
          },
          "shopkeepers": [],
          "_id": "5f7a0e3bc1dec401093b0f43",
          "name": "Débora",
          "role": "delivery",
          "createdAt": "2020-10-04T18:02:35.584Z",
          "updatedAt": "2020-10-04T18:02:35.584Z",
          "__v": 0
        },
        {
          "reference": {
            "id": "43151",
            "type": "customer"
          },
          "shopkeepers": [],
          "_id": "5f7a0e33c1dec401093b0f41",
          "name": "Graziela",
          "role": "customer",
          "createdAt": "2020-10-04T18:02:27.472Z",
          "updatedAt": "2020-10-04T18:02:27.472Z",
          "__v": 0
        }
      ],
      "open": true,
      "name": "Order 500003 - Customer with delivery",
      "orderId": "500003",
      "createdAt": "2020-10-04T18:37:57.022Z",
      "updatedAt": "2020-10-04T19:00:47.297Z",
      "countMessages": 18
    }
  ]
}
```

### GET - /rooms/customer/:customerId

Rota para consultar todas as salas de conversa relacionadas a um Usuário.

*Rota apenas disponível para Administradores*

**Query Params:**
```
page: (default 1)
perPage: (default 20)
```

**Response Example:**
```
{
  "totalItems": 2,
  "currentPage": 1,
  "totalPages": 1,
  "data": [
    {
      "_id": "5f7a168531ae2d018d89b7a0",
      "members": [
        {
          "reference": {
            "id": "341",
            "type": "delivery"
          },
          "shopkeepers": [],
          "_id": "5f7a0e3bc1dec401093b0f43",
          "name": "Débora",
          "role": "delivery",
          "createdAt": "2020-10-04T18:02:35.584Z",
          "updatedAt": "2020-10-04T18:02:35.584Z",
          "__v": 0
        },
        {
          "reference": {
            "id": "43151",
            "type": "customer"
          },
          "shopkeepers": [],
          "_id": "5f7a0e33c1dec401093b0f41",
          "name": "Graziela",
          "role": "customer",
          "createdAt": "2020-10-04T18:02:27.472Z",
          "updatedAt": "2020-10-04T18:02:27.472Z",
          "__v": 0
        }
      ],
      "open": true,
      "name": "Order 500003 - Customer with delivery",
      "createdAt": "2020-10-04T18:37:57.022Z",
      "updatedAt": "2020-10-04T19:00:47.297Z",
      "countMessages": 18
    }
  ]
}
```

### GET - /rooms/shop/:shopId

Rota para consultar todas as salas de conversa relacionadas a um Restaurante.

*Rota apenas disponível para Administradores*

**Query Params:**
```
page: (default 1)
perPage: (default 20)
```

**Response Example:**
```
{
  "totalItems": 1,
  "currentPage": 1,
  "totalPages": 1,
  "data": [
    {
      "_id": "5f7a271b4e92910253072be4",
      "members": [
        {
          "reference": {
            "id": "43151",
            "type": "customer"
          },
          "shopkeepers": [],
          "_id": "5f7a0e33c1dec401093b0f41",
          "name": "Graziela",
          "role": "customer",
          "createdAt": "2020-10-04T18:02:27.472Z",
          "updatedAt": "2020-10-04T18:02:27.472Z",
          "__v": 0
        },
        {
          "reference": {
            "id": "375",
            "type": "shop"
          },
          "shopkeepers": [],
          "_id": "5f7a0e44c1dec401093b0f45",
          "name": "Koichi",
          "role": "shop",
          "createdAt": "2020-10-04T18:02:44.314Z",
          "updatedAt": "2020-10-04T18:02:44.314Z",
          "__v": 0
        }
      ],
      "open": true,
      "name": "Order 500003 - shop with customer",
      "createdAt": "2020-10-04T19:48:43.743Z",
      "updatedAt": "2020-10-04T19:48:43.743Z",
      "countMessages": 0
    }
  ]
}
```

### GET - /rooms/delivery/:deliveryId

Rota para consultar todas as salas de conversa relacionadas a um Entregador.

*Rota apenas disponível para Administradores*

**Query Params:**
```
page: (default 1)
perPage: (default 20)
```

**Response Example:**
```
{
  "totalItems": 1,
  "currentPage": 1,
  "totalPages": 1,
  "data": [
    {
      "_id": "5f7a168531ae2d018d89b7a0",
      "members": [
        {
          "reference": {
            "id": "341",
            "type": "delivery"
          },
          "shopkeepers": [],
          "_id": "5f7a0e3bc1dec401093b0f43",
          "name": "Débora",
          "role": "delivery",
          "createdAt": "2020-10-04T18:02:35.584Z",
          "updatedAt": "2020-10-04T18:02:35.584Z",
          "__v": 0
        },
        {
          "reference": {
            "id": "43151",
            "type": "customer"
          },
          "shopkeepers": [],
          "_id": "5f7a0e33c1dec401093b0f41",
          "name": "Graziela",
          "role": "customer",
          "createdAt": "2020-10-04T18:02:27.472Z",
          "updatedAt": "2020-10-04T18:02:27.472Z",
          "__v": 0
        }
      ],
      "open": true,
      "name": "Order 500003 - Customer with delivery",
      "createdAt": "2020-10-04T18:37:57.022Z",
      "updatedAt": "2020-10-04T19:00:47.297Z",
      "countMessages": 18
    }
  ]
}
```

### GET - /rooms/order/:orderId

Rota para consultar todas as salas de conversa relacionadas a um Pedido.

*Rota apenas disponível para Administradores*

**Query Params:**
```
page: (default 1)
perPage: (default 20)
```

**Response Example:**
```
{
  "totalItems": 2,
  "currentPage": 1,
  "totalPages": 1,
  "data": [
    {
      "_id": "5f7a168531ae2d018d89b7a0",
      "members": [
        {
          "reference": {
            "id": "341",
            "type": "delivery"
          },
          "shopkeepers": [],
          "_id": "5f7a0e3bc1dec401093b0f43",
          "name": "Débora",
          "role": "delivery",
          "createdAt": "2020-10-04T18:02:35.584Z",
          "updatedAt": "2020-10-04T18:02:35.584Z",
          "__v": 0
        },
        {
          "reference": {
            "id": "43151",
            "type": "customer"
          },
          "shopkeepers": [],
          "_id": "5f7a0e33c1dec401093b0f41",
          "name": "Graziela",
          "role": "customer",
          "createdAt": "2020-10-04T18:02:27.472Z",
          "updatedAt": "2020-10-04T18:02:27.472Z",
          "__v": 0
        }
      ],
      "open": true,
      "name": "Order 500003 - Customer with delivery",
      "orderId": "500003",
      "createdAt": "2020-10-04T18:37:57.022Z",
      "updatedAt": "2020-10-04T19:00:47.297Z",
      "countMessages": 18
    }
  ]
}
```

### GET - /room/:roomId

Rota para consultar detalhes de uma Sala específica a partir do ID

**roomId:** ID da sala que deseja consultar

**Response Example:**
```
{
  "_id": "5f7a168531ae2d018d89b7a0",
  "members": [
    {
      "reference": {
        "id": "341",
        "type": "delivery"
      },
      "shopkeepers": [],
      "_id": "5f7a0e3bc1dec401093b0f43",
      "name": "Débora",
      "role": "delivery",
      "createdAt": "2020-10-04T18:02:35.584Z",
      "updatedAt": "2020-10-04T18:02:35.584Z",
      "__v": 0
    },
    {
      "reference": {
        "id": "43151",
        "type": "customer"
      },
      "shopkeepers": [],
      "_id": "5f7a0e33c1dec401093b0f41",
      "name": "Graziela",
      "role": "customer",
      "createdAt": "2020-10-04T18:02:27.472Z",
      "updatedAt": "2020-10-04T18:02:27.472Z",
      "__v": 0
    }
  ],
  "open": true,
  "name": "Order 500003 - Customer with delivery",
  "createdAt": "2020-10-04T18:37:57.022Z",
  "updatedAt": "2020-10-04T19:00:47.297Z",
  "countMessages": 18
}
```

### GET - /room/order/:orderId/shop

Rota para pegar as informações básicas da sala do chat com o Restaurante, a partir do ID do Pedido. Caso a sala do chat não exista, a mesma será criada.

*Rota apenas disponível para Usuários e Entregadores*

**Response Example:**
```
{
  "members": [
    {
      "reference": {
        "id": "43151",
        "type": "customer"
      },
      "shopkeepers": [],
      "_id": "5f7a0e33c1dec401093b0f41",
      "name": "Graziela",
      "role": "customer",
      "createdAt": "2020-10-04T18:02:27.472Z",
      "updatedAt": "2020-10-04T18:02:27.472Z",
      "__v": 0
    },
    {
      "reference": {
        "id": "375",
        "type": "shop"
      },
      "shopkeepers": [],
      "_id": "5f7a0e44c1dec401093b0f45",
      "name": "Koichi",
      "role": "shop",
      "createdAt": "2020-10-04T18:02:44.314Z",
      "updatedAt": "2020-10-04T18:02:44.314Z",
      "__v": 0
    }
  ],
  "messages": [],
  "open": true,
  "_id": "5f7a271b4e92910253072be4",
  "type": "customer_order",
  "name": "Order 500003 - shop with customer",
  "orderId": "500003",
  "createdAt": "2020-10-04T19:48:43.743Z",
  "updatedAt": "2020-10-04T19:48:43.743Z",
  "__v": 0
}
```

### GET - /room/order/:orderId/customer

Rota para pegar as informações básicas da sala do chat com o Usuário, a partir do ID do Pedido. Caso a sala do chat não exista, a mesma será criada.

*Rota apenas disponível para Restaurantes e Entregadores*

**Response Example:**
```
{
  "members": [
    {
      "reference": {
        "id": "341",
        "type": "delivery"
      },
      "shopkeepers": [],
      "_id": "5f7a0e3bc1dec401093b0f43",
      "name": "Débora",
      "role": "delivery",
      "createdAt": "2020-10-04T18:02:35.584Z",
      "updatedAt": "2020-10-04T18:02:35.584Z",
      "__v": 0
    },
    {
      "reference": {
        "id": "43151",
        "type": "customer"
      },
      "shopkeepers": [],
      "_id": "5f7a0e33c1dec401093b0f41",
      "name": "Graziela",
      "role": "customer",
      "createdAt": "2020-10-04T18:02:27.472Z",
      "updatedAt": "2020-10-04T18:02:27.472Z",
      "__v": 0
    }
  ],
  "messages": [],
  "open": true,
  "_id": "5f7a168531ae2d018d89b7a0",
  "type": "customer_order_delivery",
  "name": "Order 500003 - Customer with delivery",
  "orderId": "500003",
  "createdAt": "2020-10-04T18:37:57.022Z",
  "updatedAt": "2020-10-04T19:00:47.297Z",
  "__v": 18
}
```

### GET - /room/order/:orderId/delivery

Rota para pegar as informações básicas da sala do chat com o Entregador, a partir do ID do Pedido. Caso a sala do chat não exista, a mesma será criada.

*Rota apenas disponível para Restaurantes e Usuários*

**Response Example:**
```
{
  "members": [
    {
      "reference": {
        "id": "341",
        "type": "delivery"
      },
      "shopkeepers": [],
      "_id": "5f7a0e3bc1dec401093b0f43",
      "name": "Débora",
      "role": "delivery",
      "createdAt": "2020-10-04T18:02:35.584Z",
      "updatedAt": "2020-10-04T18:02:35.584Z",
      "__v": 0
    },
    {
      "reference": {
        "id": "43151",
        "type": "customer"
      },
      "shopkeepers": [],
      "_id": "5f7a0e33c1dec401093b0f41",
      "name": "Graziela",
      "role": "customer",
      "createdAt": "2020-10-04T18:02:27.472Z",
      "updatedAt": "2020-10-04T18:02:27.472Z",
      "__v": 0
    }
  ],
  "messages": [],
  "open": true,
  "_id": "5f7a168531ae2d018d89b7a0",
  "type": "customer_order_delivery",
  "name": "Order 500003 - Customer with delivery",
  "orderId": "500003",
  "createdAt": "2020-10-04T18:37:57.022Z",
  "updatedAt": "2020-10-04T19:00:47.297Z",
  "__v": 18
}
```

### GET - /room/customer/:customerId

Rota para pegar as informações básicas da sala do chat entre um Administrador e um Usuário.

*Rota apenas disponível para Administradores*

**Response Example:**
```
{
  "members": [
    {
      "reference": {
        "id": "301",
        "type": "support"
      },
      "shopkeepers": [],
      "_id": "5f779e2f18fd9d0085ce5628",
      "name": "Mata Fome Lanches",
      "role": "support",
      "createdAt": "2020-10-02T21:39:59.756Z",
      "updatedAt": "2020-10-02T21:39:59.756Z",
      "__v": 0
    },
    {
      "reference": {
        "id": "43149",
        "type": "customer"
      },
      "shopkeepers": [],
      "_id": "5f79f5c4b185d3009093a04b",
      "name": "Patrícia",
      "role": "customer",
      "createdAt": "2020-10-04T16:18:12.194Z",
      "updatedAt": "2020-10-04T16:18:12.194Z",
      "__v": 0
    }
  ],
  "messages": [],
  "open": true,
  "_id": "5f7a4252a7c2c803a4f24fce",
  "type": "support_customer",
  "name": "Support with customer 43149",
  "createdAt": "2020-10-04T21:44:50.600Z",
  "updatedAt": "2020-10-04T21:44:50.600Z",
  "__v": 0
}
```

### GET - /room/shop/:shopId

Rota para pegar as informações básicas da sala do chat entre um Administrador e um Restaurante.

*Rota apenas disponível para Administradores*

**Response Example:**
```
{
  "members": [
    {
      "reference": {
        "id": "301",
        "type": "support"
      },
      "shopkeepers": [],
      "_id": "5f779e2f18fd9d0085ce5628",
      "name": "Mata Fome Lanches",
      "role": "support",
      "createdAt": "2020-10-02T21:39:59.756Z",
      "updatedAt": "2020-10-02T21:39:59.756Z",
      "__v": 0
    },
    {
      "reference": {
        "id": "326",
        "type": "shop"
      },
      "shopkeepers": [],
      "_id": "5f7a4f91d7d34904bdbf2df1",
      "name": "Galeão",
      "role": "shop",
      "createdAt": "2020-10-04T22:41:21.257Z",
      "updatedAt": "2020-10-04T22:41:21.257Z",
      "__v": 0
    }
  ],
  "messages": [],
  "open": true,
  "_id": "5f7a4f91d7d34904bdbf2df2",
  "type": "support_shop",
  "name": "Support with Shop 326",
  "createdAt": "2020-10-04T22:41:21.265Z",
  "updatedAt": "2020-10-04T22:41:21.265Z",
  "__v": 0
}
```

### GET - /room/delivery/:deliveryId

Rota para pegar as informações básicas da sala do chat entre um Administrador e um Entregador.

*Rota apenas disponível para Administradores*

**Response Example:**
```
{
  "members": [
    {
      "reference": {
        "id": "301",
        "type": "support"
      },
      "shopkeepers": [],
      "_id": "5f779e2f18fd9d0085ce5628",
      "name": "Mata Fome Lanches",
      "role": "support",
      "createdAt": "2020-10-02T21:39:59.756Z",
      "updatedAt": "2020-10-02T21:39:59.756Z",
      "__v": 0
    },
    {
      "reference": {
        "id": "326",
        "type": "delivery"
      },
      "shopkeepers": [],
      "_id": "5f7a4fb2d7d34904bdbf2df3",
      "name": "MARCUS",
      "role": "delivery",
      "createdAt": "2020-10-04T22:41:54.134Z",
      "updatedAt": "2020-10-04T22:41:54.134Z",
      "__v": 0
    }
  ],
  "messages": [],
  "open": true,
  "_id": "5f7a4fb2d7d34904bdbf2df4",
  "type": "support_delivery",
  "name": "Support with Delivery 326",
  "createdAt": "2020-10-04T22:41:54.139Z",
  "updatedAt": "2020-10-04T22:41:54.139Z",
  "__v": 0
}
```

### GET - /room/messages/:roomId

Rota para pegar o histórico de mensagens de uma sala de chat.

**Response Example:**
```
[
  {
    "_id": "5f81f7348bd3b20029f2c560",
    "viewed": true,
    "from": {
      "reference": {
        "id": "17443",
        "type": "customer"
      },
      "_id": "5f77a4055d1d9f00a6e256e4",
      "name": "Leonardo"
    },
    "room": "5f81f72e8bd3b20029f2c55f",
    "fromCurrentUser": true,
    "createdAt": "2020-10-10T18:02:28.247Z",
    "updatedAt": "2020-10-10T18:02:28.350Z"
  },
]
```

**Observação:** O campo ***fromCurrentUser*** informa se o usuário que realizou a requisição foi o mesmo que enviou a mensagem ou não.

## Socket Server

O servidor de Socket estará rodando na mesma URL que API e poderá sera acessado adicionando um "/chat" no final da URL. Por exemplo: **http://localhost:3333/chat**

Mais instruções sobre conexão através da documentação do [SocketIO](https://socket.io/docs/client-api/).

### Autenticação

Para realizar a conexão com o servidor Socket, é necessário passar os mesmos token enviados para a API: **Authorization** e **Identification**, através dos Query Params.

### Eventos Emitidos

#### Event Name: newMessage

Evento emitido sempre que uma nova mensagem é enviada para alguém que está conectado no servidor.

**Payload:**
```
{
  "_id": "5f81f7348bd3b20029f2c560",
  "viewed": true,
  "from": {
    "reference": {
      "id": "17443",
      "type": "customer"
    },
    "_id": "5f77a4055d1d9f00a6e256e4",
    "name": "Leonardo"
  },
  "room": "5f81f72e8bd3b20029f2c55f",
  "fromCurrentUser": true,
  "createdAt": "2020-10-10T18:02:28.247Z",
  "updatedAt": "2020-10-10T18:02:28.350Z"
}
```

**Observação:** O campo ***fromCurrentUser*** informa se o usuário que está conectado recebendo este evento foi o mesmo que enviou a mensagem ou não.

#### Event Name: messagesViewed

Evento emitido sempre que as mensagens de uma sala forem marcadas como visualizadas.

**Payload:**
```
{
  roomId: room._id,
}
```

#### Event Name: error

Evento emitido sempre que ocorre algum erro inesperado durante a conexão com o servidor.

**Payload:**
```
{
  type: "unexpected_error" || "authorization_error",
  message: "Explicação do erro"
}
```

### Eventos esperados

#### Event Name: newMessage

Evento esperado para quando uma nova mensagem for enviada dentro de alguma Sala de Chat.

**Payload:**
```
{
  roomId: Id da Sala do Chat que o usuário enviou a mensagem,
  message: "Conteúdo da mensagem"
}
```

#### Event Name: messagesViewed

Evento esperado para quando um usuário visualizar as mensagens de um chat específico.

**Payload:**
```
{
  roomId: room._id,
}
```

## Observações

O projeto utiliza o TOKEN_IDENTIFICATION para identificar qual é o tipo de usuário conectado e evitando o envio de informações extras em todas as rotas. Ele é obrigatório durante a autenticação e pode ser exposto nas páginas.

Depois que uma Sala de Chat fica mais de 24h sem receber nenhuma mensagem, ela é automaticamente marcada como "fechada" e começa a ser desconsiderada pelo sistema. As rotas a seguir são as únicas que exibem rotas marcadas como "fechadas":
- [Find rooms by Customer (exclusivo Admin)](#get---roomscustomercustomerid)
- [Find rooms by Shop (exclusivo Admin)](#get---roomsshopshopid)
- [Find rooms by Delivery Man (exclusivo Admin)](#get---roomsdeliverydeliveryid)
- [Find rooms by Order (exclusivo Admin)](#get---roomsorderorderid)
